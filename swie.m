- (void)loadView
{
    UIView *aView = [[[UIView alloc] initWithFrame:[UIScreen mainScreen].bounds] autorelease];
    aView.backgroundColor = [UIColor whiteColor];
    self.view = aView;
    [aView release];
    _imageView=[[UIImageView alloc]initWithFrame:CGRectMake(10, 20, 300, 400)];
    _imageView.userInteractionEnabled=YES;
    _imageView.image=[UIImage imageNamed:@"h1.jpeg"];
    [self.view addSubview:_imageView];
    [_imageView release];
  
    //轻扫手势
    UISwipeGestureRecognizer *swi=nil;
    swi=[[UISwipeGestureRecognizer alloc]initWithTarget:self
                                                 action:@selector(swie:)];
    [self.view addGestureRecognizer:swi];
    [swi release];
}
 
-(void)swie:(UISwipeGestureRecognizer *)Ager{
    _imageView.image=[UIImage imageNamed:@"h2.jpeg"];
    NSLog(@"%s",__func__);
}